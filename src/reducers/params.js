import * as types from '@actions/types';

const initialState = {
    isLoggedIn: false,
    loading: false,
};

const params = (state = initialState, action) => {
    switch (action.type) {
        case types.LOADING_START:
            return {
                ...state,
                loading: true
            };

        case types.LOADING_END:
            return {
                ...state,
                loading: false
            };

        case types.LOGIN:
            return {
                ...state,
                loading: true
            };
        case types.LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                errorMsg: null,
                ...action.payload || {}
            };

        case types.LOGIN_FAILED:
            return {
                ...state,
                isLoggedIn: false,
                ...action.payload || {}
            };

        case types.LOGOUT:
            return initialState;

        default:
            return { ...state, ...action.payload || {}};
    }
};

export default params;
