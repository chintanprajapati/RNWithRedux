
//action
export const CUSTOM_ACTION = 'CUSTOM_ACTION';
export const LOGOUT = 'LOGOUT';

export const LOADING = 'LOADING';
export const LOADING_START = 'LOADING_START';
export const LOADING_END = 'LOADING_END';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';

export const RESET_MESSAGES = 'RESET_MESSAGES';
export const SET_PUBLIC_IP = 'SET_PUBLIC_IP';

// Navigation
export const NAVIGATE_DASHBOARD = 'NAVIGATE_DASHBOARD';
export const NAVIGATE_BACK = 'NAVIGATE_BACK';
