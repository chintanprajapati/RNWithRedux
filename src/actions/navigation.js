import {
    AsyncStorage
} from 'react-native';
import * as types from '@actions/types'

import { getCoinMarketCapDetail, getProfile } from '@actions/account';

export const init = () => {
    return async (dispatch,getState) => {
        dispatch({ type: types.LOADING_START });
        // to-do here
        dispatch({ type: types.LOADING_END });
    }
}

export const login = (email,password) => {
    return (dispatch,getState) => {
        dispatch({ type: types.LOGIN });
        // to-do here
        // Api call
        dispatch({ type: types.LOGIN_SUCCESS });
        // dispatch({ type: types.LOGIN_FAILED });
    }
}
